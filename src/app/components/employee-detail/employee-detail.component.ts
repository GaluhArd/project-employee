import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { FakeDataService } from '../../services/fake-data.service'
import Employee from 'src/app/interfaces/employee.interface'
import { Location } from '@angular/common'


@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss'],
})
export class EmployeeDetailComponent implements OnInit {
  employee: Employee | undefined;

  constructor(private route: ActivatedRoute, private fakeDataService: FakeDataService,  private location: Location) {}

  ngOnInit() {
    const employeeId = this.route.snapshot.paramMap.get('id');
    if (employeeId) {
      this.employee = this.fakeDataService.getEmployeeById(parseInt(employeeId));
    }
  }

  goBack() {
    this.location.back();
  }
}
