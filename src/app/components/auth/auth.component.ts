import { Component } from '@angular/core'
import { AuthService } from '../../services/auth.service'
import * as bcrypt from 'bcryptjs'; // Import bcryptjs
import { Router } from '@angular/router'

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent {
  username = ''

  constructor(private authService: AuthService, private router: Router) {}

  login() {
    //  i am not using password because in employee attribute example there is no password so in here i will just use username
    const isLogin = this.authService.login(this.username);
    if (isLogin) {
      this.router.navigate(['/employee-list']);
    }
  }
}
