import { Component, OnInit } from '@angular/core';
import { FakeDataService } from '../../services/fake-data.service'
import Employee from '../../interfaces/employee.interface'
import { Subject, of, BehaviorSubject } from 'rxjs'
import { debounceTime, distinctUntilChanged, takeUntil, switchMap} from 'rxjs/operators'
import { Router, ActivatedRoute } from '@angular/router'


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
})
export class EmployeeListComponent implements OnInit {
  employees: Employee[] = [];
  filteredEmployees: Employee[] = [];
  currentPage = 1;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 20, 50];
  totalPages = 0;
  sortBy: keyof Employee = 'firstName';
  sortAscending = true;
  searchTerm = '';
  private destroy$ = new Subject<void>();
  searchInput$ = new BehaviorSubject<string>('');
  prevSearchTerm = '';
  isDelete = false;
  isUpdate = false;


  constructor(private fakeDataService: FakeDataService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.totalPages = Math.ceil(this.fakeDataService.getEmployees().length / this.pageSize);
    this.route.queryParamMap.subscribe((queryParams) => {
      const searchTerm = queryParams.get('search') || '';
      const sortColumn = queryParams.get('sortColumn') || 'firstName';
      const sortDirection = queryParams.get('sortDirection') || 'asc';
      const pageSize = Number(queryParams.get('pageSize')) || 10;
      const page = Number(queryParams.get('page')) || 1;
      this.prevSearchTerm = searchTerm
      this.currentPage = page
      this.sortBy = sortColumn as keyof Employee;
      this.sortAscending = sortDirection === 'asc';
      this.pageSize = pageSize;
      this.searchInput$.next(searchTerm);
      this.setupSearchDebounce();
    })
    this.loadPage(this.currentPage);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  loadPage(page?: number) {
    this.currentPage = page ? page : this.currentPage;
    if (this.searchTerm) {
      this.fakeDataService.sortEmployeesByProperty(this.sortBy, this.sortAscending, this.filteredEmployees);
      this.employees = this.fakeDataService.getEmployeesPage(this.currentPage, this.pageSize, this.filteredEmployees);
    } else {
      this.fakeDataService.sortEmployeesByProperty(this.sortBy, this.sortAscending);
      this.employees = this.fakeDataService.getEmployeesPage(this.currentPage, this.pageSize);
    }
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { page: this.currentPage, sortColumn: this.sortBy, sortDirection: this.sortAscending ? 'asc' : 'desc', pageSize: this.pageSize},
      queryParamsHandling: 'merge',
    })
  }

  toggleSortOrder() {
    this.sortAscending = !this.sortAscending;
    this.sortData();
  }

  onChangeSort() {
    this.sortData();
  }

  setupSearchDebounce() {
    this.searchInput$
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        switchMap((searchTerm) => {
          const allEmployees = this.fakeDataService.getEmployees();
          if (searchTerm) {  
            this.searchTerm = searchTerm;
            const filteredEmployees = allEmployees.filter(
              (employee) =>
                employee.firstName.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
                employee.lastName.toLowerCase().includes(this.searchTerm.toLowerCase())
            );
            this.filteredEmployees = filteredEmployees;    
            this.totalPages = Math.ceil(filteredEmployees.length / this.pageSize);
            if (this.prevSearchTerm !== searchTerm) {
              this.prevSearchTerm = searchTerm;
              this.loadPage(1);
              this.updateQueryParameters(true);
            } else {
              this.loadPage();
              this.updateQueryParameters(false);
            }
            return of(filteredEmployees);
          } else {
            this.filteredEmployees = allEmployees;
            this.totalPages = Math.ceil(allEmployees.length / this.pageSize);
            this.loadPage();
            this.updateQueryParameters(false);
            return of(allEmployees);
          }
        }),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {})
  }

  onSearchInput(inputValue: string) {
    this.searchInput$.next(inputValue);
  }

  onEnterPressed(searchTerm: string) {
    this.searchInput$.next(searchTerm);
  }

  onDelete() {
    const text = 'Are you sure you want to delete this employee ? ';
    this.isDelete = true;
    if (confirm(text) == true) {
      setTimeout(() => {
        this.isDelete = false
      }, 3000);
    }
  }

  onUpdate() {
    this.isUpdate = true;
    setTimeout(() => {
      this.isUpdate = false;
    }, 3000)
  }

  private updateQueryParameters(isChange:boolean) {
    let queryParam = {search: this.searchTerm, page:this.currentPage};
    if(isChange){
      queryParam = {...queryParam, page:1};
    }
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: queryParam ,
      queryParamsHandling: 'merge',
    });
  }

  sortData() {
    this.loadPage(1);
  }
  
  onChangePageSize() {
    this.loadPage(1);
  }
}
