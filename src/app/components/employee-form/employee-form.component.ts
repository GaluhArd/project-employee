import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Location } from '@angular/common'
import { Router } from '@angular/router'
import { FakeDataService } from '../../services/fake-data.service';
import Employee from 'src/app/interfaces/employee.interface';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss'],
})
export class EmployeeFormComponent implements OnInit {
  employeeForm!: FormGroup;
  availableGroups: string[] = [
    'Group A',
    'Group B',
    'Group C',
    'Sales',
    'IT',
    'Product',
    'HR',
    'GA',
    'Corporate',
    'Head',
  ];
  availStatus: string[] = ['Inactive', 'Active', 'On Leave'];
  filteredGroups: string[] = [];
  hasFocus: boolean = false;
  selectedGroup: string | null = null;

  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private fakeDataService: FakeDataService,
    private router: Router
  ) {}

  ngOnInit() {
    this.employeeForm = this.formBuilder.group({
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: ['', Validators.required],
      basicSalary: [0, Validators.required],
      status: ['', Validators.required],
      group: [null, Validators.required],
      description: ['', Validators.required],
    })
  }

  onSubmit() {
    if (this.employeeForm.valid) {
      const newEmployee: Employee = {
        id: 0,
        username: this.employeeForm.value.username,
        firstName: this.employeeForm.value.firstName,
        lastName: this.employeeForm.value.lastName,
        email: this.employeeForm.value.email,
        birthDate: this.employeeForm.value.birthDate,
        basicSalary: this.employeeForm.value.basicSalary,
        status: this.employeeForm.value.status,
        group: this.employeeForm.value.group,
        description: this.employeeForm.value.description,
      }

      this.fakeDataService.addEmployee(newEmployee)
      this.employeeForm.reset()
      this.router.navigate(['/employee-list']);
    }
  }

  goBack() {
    this.location.back();
  }

  getCurrDate() {
    return new Date().toISOString().split('T')[0];
  }

  filterGroups(event?: any) {
    if (event) {
      const searchTerm = (event.target as HTMLInputElement).value;
      this.filteredGroups = this.availableGroups.filter((group) =>
        group.toLowerCase().includes(searchTerm.toLowerCase())
      );
    } else {
      this.filteredGroups = this.availableGroups;
    }
  }

  selectGroup(group: string) {
    this.employeeForm.get('group')?.setValue(group);
    this.selectedGroup = group;
    this.filteredGroups = []; 
  }

  toggleFocus() {
    this.hasFocus = !this.hasFocus;
    this.filterGroups();
  }

  clearFilteredGroups() {
    this.hasFocus = false;
    this.filteredGroups = [];
  }

  get emailControl() {
    return this.employeeForm.get('email');
  }
}
