import { Injectable } from '@angular/core'
import * as bcrypt from 'bcryptjs' // Import bcryptjs
import {FakeDataService} from '../services/fake-data.service'

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private isAuthenticated = false;
  private simulatedHashedPass = '';

  constructor(private fakeDataService: FakeDataService) {}

  login(username: string): boolean {
    // i will simulate the compared username with local dummy username that been hash. Because in my opinion i should get the hash from backend or jwt i just supply some password and username to the backend from api.
    // in here i am not simulate jwt
    // i will just save the hash dummy username. as i said before i think if my username and the hash from backend correct than the hash will be save in local. but if you want more secure should use jwt for now i will just use bcrypt
    const employees = this.fakeDataService.getEmployees();
    const indexUser = employees.findIndex((user) => user.username === username);
    if (indexUser != -1) {
      const salt = bcrypt.genSaltSync(10);
      this.simulatedHashedPass = bcrypt.hashSync(employees[indexUser].username, salt);
      this.isAuthenticated = bcrypt.compareSync(username, this.simulatedHashedPass);
      if (this.isAuthenticated) {
        localStorage.setItem('hashedPassword', this.simulatedHashedPass);
      }
      return this.isAuthenticated;
    } else {
      alert('User not Found Please try diffrent user')
      return (this.isAuthenticated = false);
    }
  }

  logout() {
    localStorage.removeItem('hashedPassword');
    this.isAuthenticated = false;
  }

  isLoggedIn() {
    const token = localStorage.getItem('hashedPassword');
    if (token) {
      this.isAuthenticated = true;
    } else {
      this.isAuthenticated = false;
    }
    return this.isAuthenticated;
  }
}
