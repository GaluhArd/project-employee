import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {
  transform(value: any, format: string = 'mediumDate'): string {
    if (value === null) {
      return 'No date available';
    }

    const datePipe = new DatePipe('id-ID');
    return datePipe.transform(value, format) || '';
  }
}
