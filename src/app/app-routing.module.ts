import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { AuthGuard } from './components/auth/auth.guard'
import { EmployeeListComponent } from './components/employee-list/employee-list.component'
import { AuthComponent } from './components/auth/auth.component'
import { EmployeeFormComponent } from './components/employee-form/employee-form.component'
import { EmployeeDetailComponent } from './components/employee-detail/employee-detail.component'

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: AuthComponent },
  { path: 'employee-list', component: EmployeeListComponent, canActivate: [AuthGuard] },
  { path: 'add-employee', component: EmployeeFormComponent },
  { path: 'employee/:id', component: EmployeeDetailComponent },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
