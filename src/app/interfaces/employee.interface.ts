interface Employee {
  id:number,
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: string;
  basicSalary: number;
  status: string;
  group: string;
  description: string;
}

// birtdate and description use string because in real application usually frontend will get date a string


export default Employee
