import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { AuthComponent } from './components/auth/auth.component';
import { EmployeeDetailComponent } from './components/employee-detail/employee-detail.component';
import { EmployeeFormComponent } from './components/employee-form/employee-form.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RupiahPipe } from './pipe/rupiah.pipe';
import { DateFormatPipe } from './pipe/date.pipe'
import { LOCALE_ID } from '@angular/core'
import { registerLocaleData } from '@angular/common'
import localeId from '@angular/common/locales/id'

registerLocaleData(localeId)

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    AuthComponent,
    EmployeeDetailComponent,
    EmployeeFormComponent,
    RupiahPipe,
    DateFormatPipe,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule],
  providers: [{ provide: LOCALE_ID, useValue: 'id-ID' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
